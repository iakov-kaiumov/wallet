package com.tinkoff.sirius.koshelok.converters;

import com.tinkoff.sirius.koshelok.dto.PersonDto;
import com.tinkoff.sirius.koshelok.models.Person;
import org.springframework.stereotype.Component;

@Component
public class PersonConverter {
    public Person convert(PersonDto dto) {
        return new Person()
                .setId(dto.getId())
                .setEmail(dto.getEmail())
                .setBalance(dto.getBalance())
                .setIncome(dto.getIncome())
                .setSpendings(dto.getSpendings());
    }

    public PersonDto convert(Person person) {
        return new PersonDto()
                .setId(person.getId())
                .setEmail(person.getEmail())
                .setBalance(person.getBalance())
                .setIncome(person.getIncome())
                .setSpendings(person.getSpendings());

    }

}
