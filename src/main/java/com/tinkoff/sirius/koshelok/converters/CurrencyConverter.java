package com.tinkoff.sirius.koshelok.converters;

import com.tinkoff.sirius.koshelok.dto.CurrencyDto;
import com.tinkoff.sirius.koshelok.models.Currency;
import org.springframework.stereotype.Component;

@Component
public class CurrencyConverter {
    public CurrencyDto convert(Currency currency) {
        return new CurrencyDto()
                .setCode(currency.getCode())
                .setValue(currency.getValue())
                .setSymbol(currency.getSymbol())
                .setShortDescription(currency.getShortDescription())
                .setFullDescription(currency.getFullDescription());
    }
}
