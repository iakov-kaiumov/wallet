package com.tinkoff.sirius.koshelok.converters;

import com.tinkoff.sirius.koshelok.dto.WalletDto;
import com.tinkoff.sirius.koshelok.dto.WalletDtoResponse;
import com.tinkoff.sirius.koshelok.models.Wallet;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class WalletConverter {

    public Wallet convert(WalletDto dto) {
        return new Wallet()
                .setId(dto.getId())
                .setIsHidden(dto.getIsHidden())
                .setName(dto.getName())
                .setIncome(dto.getIncome())
                .setAmountLimit(dto.getAmountLimit())
                .setBalance(dto.getBalance())
                .setSpendings(dto.getSpendings())
                .setCurrency(dto.getCurrency());
    }

    public WalletDto convert(Wallet wallet) {
        var walletDto = new WalletDto()
                .setId(wallet.getId())
                .setIsHidden(wallet.getIsHidden())
                .setName(wallet.getName())
                .setIncome(wallet.getIncome())
                .setAmountLimit(wallet.getAmountLimit())
                .setBalance(wallet.getBalance())
                .setSpendings(wallet.getSpendings())
                .setCurrency(wallet.getCurrency());
        if (wallet.getAmountLimit().equals(BigDecimal.ZERO)) {
            walletDto.setLimitExceeded(false);
        } else {
            walletDto.setLimitExceeded(wallet.getSpendings()
                    .compareTo(wallet.getAmountLimit()) >= 0);
        }
        return walletDto;
    }

    public WalletDtoResponse convertEx(Wallet wallet) {
        return new WalletDtoResponse()
                .setId(wallet.getId())
                .setIsHidden(wallet.getIsHidden())
                .setName(wallet.getName())
                .setIncome(wallet.getIncome())
                .setAmountLimit(wallet.getAmountLimit())
                .setBalance(wallet.getBalance())
                .setSpendings(wallet.getSpendings())
                .setLimitExceeded(wallet.getSpendings()
                        .compareTo(wallet.getAmountLimit()) >= 0);
    }
}
