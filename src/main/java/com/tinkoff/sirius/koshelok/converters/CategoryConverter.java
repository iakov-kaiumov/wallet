package com.tinkoff.sirius.koshelok.converters;

import com.tinkoff.sirius.koshelok.dto.CategoryDto;
import com.tinkoff.sirius.koshelok.models.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryConverter {
    public Category convert(CategoryDto dto) {
        return new Category()
                .setId(dto.getId())
                .setType(dto.getType())
                .setName(dto.getName())
                .setColor(dto.getColor())
                .setIconId(dto.getIconId());
    }

    public CategoryDto convert(Category category) {
        return new CategoryDto()
                .setId(category.getId())
                .setType(category.getType())
                .setName(category.getName())
                .setColor(category.getColor())
                .setIconId(category.getIconId());
    }
}
