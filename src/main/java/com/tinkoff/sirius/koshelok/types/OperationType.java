package com.tinkoff.sirius.koshelok.types;

public enum OperationType {
    INCOME,
    SPENDING
}
