package com.tinkoff.sirius.koshelok.types;

public enum CurrencyType {
    RUB,
    USD,
    EUR,
    GBP,
    CHF,
    JPY,
    SEK
}
