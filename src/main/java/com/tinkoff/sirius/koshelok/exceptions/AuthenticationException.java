package com.tinkoff.sirius.koshelok.exceptions;

public class AuthenticationException extends RuntimeException{
    public AuthenticationException(String email) {
        super("User not found for email: " + email);
    }
}
