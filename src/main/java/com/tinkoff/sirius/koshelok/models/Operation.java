package com.tinkoff.sirius.koshelok.models;

import com.tinkoff.sirius.koshelok.types.OperationType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
@Entity(name = "operation")
@SequenceGenerator(allocationSize = 1, name = "operation_seq", sequenceName = "operation_seq")
public class Operation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operation_seq")
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;

    @Column
    @Enumerated(EnumType.STRING)
    private OperationType type;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column
    private BigDecimal balance;

    @CreatedDate
    private LocalDateTime date;
}
