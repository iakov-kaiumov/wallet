package com.tinkoff.sirius.koshelok.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tinkoff.sirius.koshelok.types.OperationType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Операция")
public class OperationRequestDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long walletId;

    private OperationType type;

    @Schema(description = "Категория операции")
    @Min(1)
    private Long categoryId;

    private BigDecimal balance;

    private LocalDateTime date;
}


