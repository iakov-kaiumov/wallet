package com.tinkoff.sirius.koshelok.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tinkoff.sirius.koshelok.converters.CurrencyConverter;
import com.tinkoff.sirius.koshelok.converters.WalletConverter;
import com.tinkoff.sirius.koshelok.exceptions.NotFoundException;
import com.tinkoff.sirius.koshelok.models.Wallet;
import com.tinkoff.sirius.koshelok.repositories.CurrencyRepository;
import com.tinkoff.sirius.koshelok.repositories.PersonRepository;
import com.tinkoff.sirius.koshelok.repositories.WalletRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WalletServiceTest {

    @Mock
    WalletRepository walletRepository;

    @Mock
    CurrencyRepository currencyRepository;

    @Mock
    PersonRepository personRepository;

    WalletService walletService;
    @Mock
    OperationService operationService;

    @BeforeEach
    public void setUp() {
        walletService = new WalletService(new WalletConverter(),
                new CurrencyConverter(),
                walletRepository,
                personRepository,
                operationService,
                currencyRepository);
    }

    @Test
    void createWallet() {
    }

    @Test
    void getWalletsByPersonId() {
    }

    @Test
    void getWalletByPersonId() {
    }

    @Test
    void updateWallet() {
    }

    @Test
    void updateWalletLimit() {
    }

    @Test
    void updateWalletName() {
    }

//    @Test
//    void deleteWalletThrowsNotFoundExceptionWhenRepoNotFindWallet() {
//        var notFoundId = 1L;
//        var notFoundEmail = "nbdjhb@mail.ru";
//        when(walletRepository.existsWalletByIdAndPersonId(notFoundId, notFoundEmail))
//                .thenReturn(false);
//
//        assertThrows(NotFoundException.class, () -> walletService.deleteWallet(notFoundId, notFoundEmail));
//    }
}